<?php
namespace Deployer;

include_once  __DIR__ . '/recipe/functions.php';

// Project name
set('application', 'projectname');

// Project repository
set('repository', 'git@gitlab.com:user/project.git');

set('default_stage', 'stage');
set('keep_releases', '3');
set('default_timeout', 100000);
set('default_stage', 'local');
set('bin/php', 'php -d memory_limit=-1');
set('locales', 'en_US');

// Configuration
set('shared_files', [
    'app/etc/env.php',
    'var/.maintenance.ip'
]);

set('shared_dirs', [
    'var/log',
    'var/backups',
    'var/report',
    'var/ProductImportExport',
    'var/importexport',
    'var/import_history',
    'pub/media',
    'pub/sitemaps',
    'pub/robots',
    'static/_cache',
]);
set('writable_dirs', [
    'var',
    'var/log',
    'var/backups',
    'var/report',
    'var/ProductImportExport',
    'var/importexport',
    'var/import_history',
    'pub/static',
    'pub/sitemaps',
    'pub/robots'
]);

set('user', function () {
    return getenv('DEPLOYMENT_USER');
});

set('branch', function () {
    // For option
    if (input()->hasOption('branch')) {
        $branch = input()->getOption('branch');
    }
    if (!$branch) {
        exec('git branch', $result);
        $branches = implode('', $result);

        preg_match('/\* (\S+)/m', $branches, $matches);
        $branch = $matches[1];
    }
    return $branch;
});

// -------------------------------------------------------------------------------------------------------------------

/**
 * Clean Magento Cache
 */
desc('Clean Magento Cache');
task('magento:cache:clean', function () {
    run("cd {{release_path}} && {{bin/php}} bin/magento cache:clean");
});

// -------------------------------------------------------------------------------------------------------------------


/**
 * Deploy Magento Static Content
 */
desc('Deploy Magento Static Content');
task('magento:deploy:assets', function () {
    $locales = explode(' ', get('locales'));
    foreach ($locales as $locale) {
        run("{{bin/php}} {{release_path}}/bin/magento setup:static-content:deploy -f $locale");
    }
});

// -------------------------------------------------------------------------------------------------------------------

/**
 * Clear pagespeed
 */
desc('Clear pagespeed');
task('pagespeed:clear', function () {
    run("rm -rf {{deploy_path}}/../nginx/pagespeed/* || true");
})->onStage(['prod']);

// -------------------------------------------------------------------------------------------------------------------

/**
 * Reload PHP
 */
desc('Reload PHP');
task('reload:php-fpm', function () {
    run('sudo /usr/sbin/service php7.2-fpm reload &> /dev/null');
})->onStage(['prod']);

// -------------------------------------------------------------------------------------------------------------------

/**
 * Reload Http
 */
desc('Reload Http');
task('reload:http', function () {
    run('sudo /usr/sbin/service nginx reload &> /dev/null');
})->onStage(['prod']);

// -------------------------------------------------------------------------------------------------------------------

/**
 * Reload Varnish
 */
desc('Reload Varnish');
task('reload:varnish', function () {
    run('sudo /usr/sbin/service varnish restart &> /dev/null');
})->onStage(['prod']);

// -------------------------------------------------------------------------------------------------------------------

/**
 * Reload services
 */
desc('Reload services');
task('reload:services', [
    'reload:php-fpm',
    'reload:http',
    'reload:varnish',
    'pagespeed:clear'
]);

// -------------------------------------------------------------------------------------------------------------------

require __DIR__ . '/recipe/tools.php';