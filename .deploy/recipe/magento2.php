<?php
namespace Deployer;

// Configuration
set('shared_files', [
    'app/etc/env.php',
    'var/.maintenance.ip',
]);
set('shared_dirs', [
    'var/log',
    'var/backups',
    'pub/media',
]);
set('writable_dirs', [
    'var',
    'pub/static',
    'pub/media',
]);
set('clear_paths', [
    'var/generation/*',
    'var/cache/*',
]);

// Tasks
desc('Compile magento di');
task('magento:compile', function () {
    run("{{bin/php}} {{release_path}}/bin/magento setup:di:compile");
    run('cd {{release_path}} && {{bin/composer}} dump-autoload -o');
});

desc('Deploy assets');
task('magento:deploy:assets', function () {
    run("{{bin/php}} {{release_path}}/bin/magento setup:static-content:deploy -f {{locales}}");
});

desc('Enable maintenance mode');
task('magento:maintenance:enable', function () {
    run("if [ -d $(echo {{deploy_path}}/current) ]; then {{bin/php}} {{deploy_path}}/current/bin/magento maintenance:enable; fi");
});

desc('Disable maintenance mode');
task('magento:maintenance:disable', function () {
    run("if [ -d $(echo {{deploy_path}}/current) ]; then {{bin/php}} {{deploy_path}}/current/bin/magento maintenance:disable; fi");
});

desc('Upgrade magento database');
task('magento:upgrade:db', function () {
    run("{{bin/php}} {{release_path}}/bin/magento setup:upgrade --keep-generated");
})->onRoles('db');

// this command is not available in magento
desc('Update magento config');
task('magento:update:config', function () {
    run("{{bin/php}} {{release_path}}/bin/magento setup:update:config");
})->onRoles('nodb');


desc('Flush Magento Cache');
task('magento:cache:flush', function () {
    run("{{bin/php}} {{release_path}}/bin/magento cache:flush");
});

desc('Flush Magento Cache');
task('magento:cache:clean', function () {
    run("{{bin/php}} {{release_path}}/bin/magento cache:clean");
});

desc('Magento2 deployment operations');
task('deploy:magento', [
    'magento:compile',
    'magento:deploy:assets',
    //'magento:maintenance:enable',
    'magento:update:config',
    'magento:upgrade:db',
    'magento:cache:flush',
    //'magento:maintenance:disable'
]);

desc('Magento2 deployment with maintenance on operations');
task('deploy:magento:maintenance', [
    'magento:compile',
    'magento:maintenance:enable',
    'magento:update:config',
    'magento:upgrade:db',
    'magento:cache:flush',
    'magento:maintenance:disable',
    'magento:deploy:assets',
]);

desc('Deploy your project with maintenance');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:magento:maintenance',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

after('deploy:failed', 'magento:maintenance:disable');
