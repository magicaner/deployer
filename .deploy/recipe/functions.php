<?php
namespace Deployer;

function getAllStores($preCommand='')
{
    $stores = run("$preCommand {{bin/php}} bin/magento store:list");
    $stores = explode("\n", $stores);

    array_shift($stores);
    $headers = array_shift($stores);
    array_shift($stores);

    array_pop($stores);

    $headers = array_map('trim',explode("|", $headers));
    $storeCodeIndex = array_search('Code', $headers);

    $result = [];
    foreach ($stores as $store) {
        $storeData = array_map('trim', explode('|', $store));
        $storeCode = $storeData[$storeCodeIndex];
        if ($storeCode == 'admin') {
            continue;
        }

        $result[] = $storeCode;
    }

    return $result;
}