<?php

namespace Deployer;

desc('Create Database Dump');
task('dump:db', function() {
    run("cd {{release_path}} && {{bin/php}} bin/magento dev:db:dump");
})
    ->onStage(['prod'])
    ->onRoles(['admin'])
;
