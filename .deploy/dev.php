<?php
namespace Deployer;

$user = 'deployer';
$keepReleases = '5';
$deployPath = '/var/www/{{application}}-dev';
$stage = 'prod';

host('production')
    ->user($user)
    ->set('branch', 'master')
    ->hostname('xxx.xxx.xxx.xx')->port('22')
    ->set('locales', 'en_US')
    ->set('deploy_path', $deployPath)
    ->set('keep_releases', $keepReleases)
    ->set('multiplexing', true)
    ->set('writable_use_sudo', true)
    ->set('writable_chmod_mode', 755)
    ->set('cleanup_use_sudo', true)
    ->set('use_relative_symlink', true)
    ->set('clear_use_sudo', true)
    ->stage($stage)
    ->roles(['app','web']);

