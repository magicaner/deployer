<?php
namespace Deployer;

$user = 'deployer';
$keepReleases = '3';
$deployPath = '/var/www/{{application}}';
$stage = 'prod';

host('production/admin')
    ->user($user)

    ->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->hostname('xxx.xxx.xxx.xx1')->port('22')

    ->set('branch', 'master')

    ->set('deploy_path', $deployPath)
    ->set('keep_releases', $keepReleases)
    ->set('multiplexing', true)
    ->set('writable_mode', 'chmod')
    ->set('writable_use_sudo', true)
    ->set('writable_chmod_mode', 775)
    ->set('cleanup_use_sudo', true)
    ->set('use_relative_symlink', true)
    ->set('clear_use_sudo', true)
    ->stage($stage)

    ->roles(['db','admin','app','web', 'web1']);
;

host('production/web1')
    ->user($user)

    ->configFile('~/.ssh/config')
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->hostname('xxx.xxx.xxx.xx2')->port('22')

    ->set('branch', 'master')

    ->set('locales', 'en_US')
    ->set('deploy_path', $deployPath)
    ->set('keep_releases', $keepReleases)
    ->set('multiplexing', true)
    ->set('writable_mode', 'chmod')
    ->set('writable_use_sudo', true)
    ->set('writable_chmod_mode', 775)
    ->set('cleanup_use_sudo', true)
    ->set('use_relative_symlink', true)
    ->set('clear_use_sudo', true)

    ->stage($stage)

    ->roles(['nodb','app','web', 'web2']);
;
