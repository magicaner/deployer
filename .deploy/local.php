<?php
namespace Deployer;

$keepReleases = '3';
$deployPath = __DIR__'/project-snapshot/';
$stage = 'local';

localhost()
    ->set('local', true)
    ->hostname('localhost')
    ->set('locales', 'en_US')
    ->set('deploy_path', $deployPath)
    ->set('keep_releases', $keepReleases)
    ->set('multiplexing', true)
    ->set('writable_use_sudo', true)
    ->set('writable_chmod_mode', 755)
    ->set('cleanup_use_sudo', true)
    ->set('use_relative_symlink', true)
    ->set('clear_use_sudo', true)
    ->stage($stage)
    ->roles(['app','web']);

