<?php
/**
 * Deploy project to server.
 * cli command: dep deploy prod
 * User 'deploy' should be able to login to remote server by using ssh key
 */
namespace Deployer;

require_once 'recipe/common.php';
require_once '.deploy/recipe/magento2.php';


include_once __DIR__ . "/.deploy/common.php";
include_once __DIR__ . "/.deploy/dev.php";
include_once __DIR__ . "/.deploy/prod.php";

after('deploy', 'reload:services');
after('rollback', 'reload:services');
